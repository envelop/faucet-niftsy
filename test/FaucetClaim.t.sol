// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Faucet} from "../src/Faucet.sol";
import {Niftsy} from "../src/Niftsy.sol";

contract FaucetTest is Test {
    Faucet public faucet;
    Niftsy public niftsy;

    function setUp() public {
        faucet = new Faucet(10_000e18);
        niftsy = new Niftsy(address(faucet));
        faucet.setToken(address(niftsy));
        vm.warp(1641070800);
    }

    function test_Claim() public {
        console2.log("Current block.timestamp is: %s", block.timestamp);
        faucet.claim();
        assertEq(niftsy.balanceOf(address(this)), faucet.distrAmount());
    }

    function test_ClaimSecond() public {
        faucet.claim();
        vm.expectRevert("Already claimed");
        faucet.claim();

        assertEq(niftsy.balanceOf(address(this)), faucet.distrAmount());
    }

    // function testFuzz_SetNumber(uint256 x) public {
    //     counter.setNumber(x);
    //     assertEq(counter.number(), x);
    // }
}

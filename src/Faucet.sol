// SPDX-License-Identifier: MIT
// NIFTSY protocol ERC20
pragma solidity ^0.8.23;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract Faucet  {
    uint256  public distrAmount;  
    IERC20 public faucetForToken;

    mapping (address => uint256) public claimDate;

    constructor(uint256 _oneClaimAmount)
    { 
        distrAmount =  _oneClaimAmount;
    }

    function claim() external  {
        if (_isEnabled(msg.sender)) {
            faucetForToken.transfer(msg.sender, distrAmount);
            claimDate[msg.sender] = block.timestamp;
        }
    }

    function setToken(address _token) external {
        require(address(faucetForToken) == address(0));
        faucetForToken = IERC20(_token);
        require(faucetForToken.balanceOf(address(this)) > 0, "Zero balance of this token");
    }

    function _isEnabled(address _user) internal view virtual returns(bool) {
        require(block.timestamp - claimDate[_user] > 1 hours, "Already claimed");
        return true;

    }
}
